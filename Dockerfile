FROM python:3.6-alpine
MAINTAINER Ernesto Crespo <ecrespo@gmail.com> 
COPY requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r /requirements.txt

RUN mkdir /app
COPY src/ /app
WORKDIR /app
#CMD ["gunicorn", "-w 4", "main:app"]
